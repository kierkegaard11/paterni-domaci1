/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domaci1;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
/**
 *
 * @author maja
 */
public class Form extends JFrame{
    
    public Form(){
    
        initComponents();
        
    }
   
    private JButton btnSum;
    private JButton btnSub;
    private JTextField txtA;
    private JTextField txtB;
    private JTextField txtResult;
    private JLabel lblA;
    private JLabel lblB;
    private JLabel lblresult;

    private void initComponents() {
       
      //inicijalizacija komponenti
       btnSub = new JButton();
       btnSum = new JButton();
       txtA = new JTextField();
       txtB = new JTextField();
       txtResult = new JTextField();
       lblA = new JLabel();
       lblB = new JLabel();
       lblresult = new JLabel();
       txtResult.setEditable(false);
       
       setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
       
       //setovanje layouta
       getContentPane().setLayout(null);
       getContentPane().setPreferredSize(new Dimension(300,300));
       //ako se ne podese dimenzije, forma se uopste ne prikazuje kada je podesen null layout
       
       //setovanje teksta komponenata
       btnSub.setText("Sub");
       btnSum.setText("Sum");
       lblA.setText("A:");
       lblB.setText("B:");
       lblresult.setText("Result:");
       
       //dodavanje komponenti na formu
       getContentPane().add(btnSub);
       btnSub.setBounds(20, 220, 72, 29);
       
       
       getContentPane().add(btnSum);
       btnSum.setBounds(130, 220, 72, 29);

       getContentPane().add(txtA);
       txtA.setBounds(100, 30, 86, 27);

       getContentPane().add(txtB);
       txtB.setBounds(100, 90, 86, 27);
       
       getContentPane().add(txtResult);
       txtResult.setBounds(100, 150, 86, 27);

       
       getContentPane().add(lblA);
       lblA.setBounds(10, 30, 49, 17);

       
       getContentPane().add(lblB);
       lblB.setBounds(10, 90, 49, 17);
       
       
       getContentPane().add(lblresult);
       lblresult.setBounds(10, 150, 49, 17);

       pack();
       
       
       //drugi nacin je da se uradi putem anonimne klase, sto je verovatno bolja praksa (?)
       /*
            btnSum.addActionListener(new ActionListener{
                public void actionPerformed(ActionEvent e){
                    btnSumActionPerformed(e);
       }
            });
       */
       btnSum.addActionListener(new MyListener());
       btnSub.addActionListener(new MyListener());
    }
    //u slucaju da smo koristili anonimnu klasu, metoda btnSumActionPerformed:
    /*
        btnSumActionPerformed(ActionEvent e){
        int a = Integer.parseInt(txtA.getText().trim());
        int b = Integer.parseInt(txtB.getText().trim());
        txtResult.setText((a+b)+" "); 
    }
    */
    private class MyListener implements ActionListener{
    
        public void actionPerformed(ActionEvent e){
             int a = Integer.parseInt(txtA.getText().trim());
             int b = Integer.parseInt(txtB.getText().trim());
             String command = e.getActionCommand();
        
             if(command.equals("Sum")){
                txtResult.setText((a+b)+" ");

             }
             if(command.equals("Sub")) {
                txtResult.setText((a-b)+" ");
             }
        }
    }
    
}

